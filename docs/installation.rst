.. highlight:: shell

============
Installation
============

From sources
------------

The sources for refscrub can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://git.passcal.nmt.edu/passoft/refscrub

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://git.passcal.nmt.edu/passoft/refscrub/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://git.passcal.nmt.edu/passoft/refscrub
.. _tarball: https://git.passcal.nmt.edu/passoft/refscrub/tarball/master
