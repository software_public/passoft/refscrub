#!/usr/bin/env python
"""
Lloyd Carothers
February 2011
Rewrite of original refscrub

Derick Hess
July 2018
Minor fixes for new build system

Michael Love
August 2018
Updates to work on both Python 2 & 3.
Code cleanup to match PEP 8.
Cleanup global vars.

Maeva Pourpoint
August 2020
Updates to work under Python 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
"""

import argparse
import struct
import sys
from os.path import basename, getsize, isfile

PROG_VERSION = '2023.2.0.0'
VERBOSE = False
EXTRACT = False
SUMMARY_FILE = 'scrubsum.txt'
PREFIX = ""
FILESIZE = 0
TABS = "\t\t\t\t"


class SN:
    def __init__(self, sn):
        global EXTRACT
        global PREFIX
        self.sn = sn
        self.count = 0
        self.dates = list()
        if EXTRACT:
            self.outfh = open(PREFIX + '.' + sn + '.scrub.ref', 'ab')
        else:
            self.outfh = None

    def look(self, year, day, hour):
        self.dates.append((year, day, hour))
        self.count += 1

    def close_fh(self):
        if self.outfh is not None:
            self.outfh.close()

    def __str__(self):
        self.dates = list(set(self.dates))
        self.dates.sort()
        start = "%0.2d:%0.3d:%0.2d" % (self.dates[0])
        end = "%0.2d:%0.3d:%0.2d" % (self.dates[-1])
        return "%7s: %s -- %s : %12d\n" % (self.sn, start, end, self.count)


class SNseen(dict):
    def look(self, sn, year, day, hour):
        ret = self.setdefault(sn, SN(sn))
        ret.look(year, day, hour)

    def close_fhs(self):
        for sn in list(self.values()):
            sn.close_fh()

    def __str__(self):
        s = "     SN: YR:DAY:HR -- YR:DAY:HR : %12s\n" % 'Good Packets'
        for sn in list(self.values()):
            s += str(sn)
        return s


class RTPacket:
    RTstruct = struct.Struct('2c1B1B2B6B2B2B')
    # Define the packet types as byte literals, since that is what the above
    # structure will cause the type field do be decoded as. Without this, they
    # will not match below.
    packet_types = (b'AD', b'CD', b'DS', b'DT',
                    b'EH', b'ET', b'OM', b'SH', b'SC')
    seen = SNseen()
    goodpkts = 0
    IOErrorCount = 0

    def __init__(self, data):
        self.data = data
        self.valid = None
        self.timestring = None
        self.ptype = None
        self.expnum = None
        self.year = None
        self.sn = None
        self.day = None
        self.hour = None
        self.min = None
        self.sec = None
        self.millisec = None
        self.bytecount = None
        self.sequence = None

    def __str__(self):
        self.settimestring()
        s = "%(sequence)s %(ptype)s %(sn)s %(timestring)s" % self.__dict__
        return s

    def write(self):
        outfh = RTPacket.seen[self.sn].outfh
        try:
            outfh.write(self.data)
        except Exception as e:
            print("Failed to write extracted data to %s" % outfh.filename)
            print(e)

    def settimestring(self):
        self.timestring = ("%(year)0.2d:%(day)0.3d:%(hour)0.2d:%(min)0.2d:"
                           "%(sec)0.2d.%(millisec)0.3d" % self.__dict__)

    def isvalid(self):
        """
        Returns True if a valid reftek packet (headers parse well and are
        valid)
        Also populates the objects attributes SN, time, etc.
        """

        global VERBOSE
        try:
            # This tup will be a byte literal.
            tup = RTPacket.RTstruct.unpack(self.data[:16])
            self.ptype = tup[0] + tup[1]
            assert self.ptype in self.packet_types, "BAD packet type"
            self.expnum = int("%0.2X" % tup[2])
            self.year = int("%0.2X" % tup[3])
            self.sn = "%0.2X%0.2X" % (tup[4], tup[5])
            assert self.sn >= '9001', "BAD SN"
            assert self.sn <= 'FFFF', "BAD SN"
            time = "%0.2X%0.2X%0.2X%0.2X%0.2X%0.2X" % tup[6:12]
            self.day, self.hour, self.min, self.sec, self.millisec = \
                int(time[:3]), int(time[3:5]), int(
                    time[5:7]), int(time[7:9]), int(time[9:])
            assert self.day <= 366, "BAD TIME"
            assert self.hour <= 24, "BAD TIME"
            assert self.min <= 60, "BAD TIME"
            assert self.sec <= 80, "BAD TIME"
            assert self.millisec <= 1000, "BAD TIME"
            self.bytecount = int("%0.2X%0.2X" % tup[12:14])
            assert self.bytecount >= 24, "BAD byte count"
            assert self.bytecount <= 1024, "BAD bytecount"
            self.sequence = int("%0.2X%0.2X" % tup[14:16])
        # not a valid packet
        except Exception as e:
            if VERBOSE:
                print(TABS + "Not a valid REFTEK packet")
                print(TABS + str(e))
            self.valid = False
            return False
        # parsed well :: valid packet
        else:
            self.valid = True
            RTPacket.seen.look(self.sn, self.year, self.day, self.hour)
            RTPacket.goodpkts += 1
            return True


def readfile(infile):
    ticker = 1
    global VERBOSE
    global EXTRACT
    while True:
        ticker += 1
        if VERBOSE is False and ticker == 10000:
            print(summary(infile))
            ticker = 1
        if VERBOSE:
            print("OFFSET: %12d" % infile.tell())
        # often disk wont read
        try:
            buffer = infile.read(1024)
        except IOError:
            print(TABS + "IOError on read")
            print(TABS + "Skipping 1MB..")
            RTPacket.IOErrorCount += 1
            infile.seek(1024 * 1024, 1)
            continue
        # End of file
        if len(buffer) < 1024:
            if VERBOSE:
                print("Partial packet")
            break
        p = RTPacket(buffer)
        valid = p.isvalid()
        # Packet is good
        if valid:
            if EXTRACT:
                p.write()
            if VERBOSE:
                print(p)
                print("<<<<<<<")
        # packet is bad
        else:
            if VERBOSE:
                print(TABS + "NOT VALID")
                print(TABS + ">>>>>>>>")
            infile.seek(-1023, 1)

    print(summary(infile))
    # commented out as when run with many files as input like a cf dir the
    # closed fh's need to be written to and isn't smart enough to be opened
    # RTPacket.seen.close_fhs()


def summary(infile):
    global FILESIZE
    offstring = "%6s: %12d\n"
    s = offstring % ("OFFSET", infile.tell())
    s += offstring % ("OF", FILESIZE)
    s += "Good packets: %d = %8.2fMB\n" % (
        RTPacket.goodpkts, RTPacket.goodpkts / 1024.0)
    s += "IOErrors: %d\n" % RTPacket.IOErrorCount
    s += str(RTPacket.seen) + '\n'
    return s


def main():
    global VERBOSE
    global EXTRACT
    global PREFIX
    global FILESIZE
    summaryfh = None
    parser = argparse.ArgumentParser(prog="refscrub",
                                     usage="%(prog)s [options] infile1 "
                                     "[ infile2 ... infileN]")
    parser.add_argument('infile', nargs='*', metavar='infile',
                        help="infile can be a REFTEK file or a raw disk "
                        "(/dev/disk1) of a CF card.")
    parser.add_argument('--version', action='version',
                        version="%(prog)s " + PROG_VERSION)
    parser.add_argument('-v', '--verbose', dest="VERBOSE", action='store_true',
                        default=False, help="Prints info about each packet, "
                        "good or bad. This will increase runtime.")
    parser.add_argument('-e', '--extract', dest='EXTRACT', action='store_true',
                        default=False, help="Writes good packets to files "
                        "named infile.SNXX.scrub.ref OR "
                        "PREFIX.SNXX.scrub.ref, if given, for each Serial "
                        "Number found. If output file exists it will append. "
                        "Be careful not to duplicate data by running more "
                        "than once on the same file in the same dir.")
    parser.add_argument('-p', '--prefix', dest='PREFIX',
                        help="Prefix of output filename. Defaults to input"
                        "filename")
    parser.add_argument('-s', '--savesum', dest='SUMMARY', action='store_true',
                        default=False, help='Appends summary to %s'
                        % SUMMARY_FILE)
    args = parser.parse_args()
    if not args.infile:
        parser.print_help()
        sys.exit(1)
    VERBOSE = args.VERBOSE
    EXTRACT = args.EXTRACT
    PREFIX = args.PREFIX
    if args.SUMMARY:
        summaryfh = open(SUMMARY_FILE, 'a')
    for infilename in args.infile:
        print("Processing: %s" % infilename)
        if not PREFIX:
            PREFIX = basename(infilename)
        print("Using prefix %s" % PREFIX)
        FILESIZE = getsize(infilename)
        # Must open the file in binary mode or else we will have unicode issues
        # when reading.
        with open(infilename, "rb") as infile:
            readfile(infile)
            if args.SUMMARY:
                summaryfh.write(infilename + ', ' + PREFIX + '\n')
                summaryfh.write(summary(infile))
        print("----------------------------------------")
    if isfile(SUMMARY_FILE):
        summaryfh.close()


if __name__ == '__main__':
    main()
