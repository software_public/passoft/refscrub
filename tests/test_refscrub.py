#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `refscrub` package."""

import argparse
import os
import unittest

from unittest.mock import patch
from refscrub.refscrub import main

TEST_DATA = "tests/test_data/020000005_0036EE80"
TEST_SUMMARY = "tests/test_data/scrubsum_test.txt"

if os.path.isfile('scrubsum.txt'):
    os.remove('scrubsum.txt')


class TestRefscrub(unittest.TestCase):
    """Tests for `refscrub` package."""

    @patch('refscrub.refscrub.sys.exit', autospec=True)
    @patch('argparse.ArgumentParser.parse_args', autospec=True)
    def test_refscrub(self, mock_parser, mock_exit):
        """Test basic functionality of refscrub"""
        mock_parser.return_value = argparse.Namespace(EXTRACT=False,
                                                      PREFIX=None,
                                                      SUMMARY=False,
                                                      VERBOSE=False,
                                                      infile=[])
        main()
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - Failed "
                        "to exercise refscrub")

        mock_parser.return_value = argparse.Namespace(EXTRACT=False,
                                                      PREFIX=None,
                                                      SUMMARY=True,
                                                      VERBOSE=False,
                                                      infile=[TEST_DATA])
        main()
        self.assertTrue(os.path.isfile('scrubsum.txt'), "Error! Summary file "
                        "not created.")
        with open("scrubsum.txt", "rt") as f_out:
            out = f_out.readlines()
        with open(TEST_SUMMARY, "rt") as f_test:
            test = f_test.readlines()
        self.assertEqual(out, test)
