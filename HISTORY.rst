=======
History
=======

2011.046 (2018-06-07)
------------------

* First release on new build system.

2018.180 (2018-06-29)
------------------

* 2nd release on new build system.

2018.204 (2018-07-23)
------------------

* Minor fixes of global variables

2018.228 (2018-09-17)
------------------

* Python 2.7 & 3.6 compatiblity.
* Code cleanup to match PEP 8.

2020.216 (2020-08-04)
------------------
* Updated to work with Python 3
* Added unit tests to ensure basic functionalities of refscrub
* Updated list of platform specific dependencies to be installed when
  installing refscrub in dev mode (see setup.py)
* Installed and tested refscrub against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda package for refscrub that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline
