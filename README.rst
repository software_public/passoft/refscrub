========
refscrub
========

* Description: Remove select packets from RT130 data

* Usage: efscrub [options] infile1 [ infile2 ... infileN]

* Free software: GNU General Public License v3 (GPLv3)
